/*!
* Start Bootstrap - Shop Item v5.0.5 (https://startbootstrap.com/template/shop-item)
* Copyright 2013-2022 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-shop-item/blob/master/LICENSE)
*/
// This file is intentionally blank
// Use this file to add JavaScript to your project

getAvatarData();

async function getAvatarData() {
    const response= await fetch('http://intensif01.ensicaen.fr:8080/fine/event/1')
    const data= await response.json();
    length=data.fine_detail.length;
    var temp="";
    for(i=0;i<length;i++)
    {
       temp+="<tr>";
       temp+="<td>"+data.fine_detail[i].date+"</td>";
       temp+="<td>"+data.fine_detail[i].fine_price+"€</td>";
       temp+="</tr>";
    }

    document.getElementById("avatar-data").innerHTML=temp;
    document.getElementById("total-avatar-fine").innerHTML=data.total_fine_price+"€";
}